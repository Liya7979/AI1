#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "state.h"
#include "fringe.h"

#define RANGE 1000000
#define PLUS_ONE 1 //(+1)
#define MINUS_ONE 2 //(-1)
#define MULT_TWO 3 //(*2)
#define MULT_THREE 4 //(*3)
#define DIV_TWO 5 //(/2)
#define DIV_THREE 6 //(/3)

/* This function prints the final path to the goal */
void printPath(Fringe parent, int goal);

/* This functions helps printPath function to display the path in the correct order */
void readReverse(int numbers[], int operators[], int length);

/* THis function inserts valid state in the given range as well as its parent and priority (relevant for PRIO mode) */
Fringe insertValidSucc(Fringe fringe, int value, int parent, int priority);

/* This function searches the path to the goal if it exists and prints stats of it*/
void search(int mode, int start, int goal);


int main(int argc, char *argv[]) {
    int start, goal, fringetype;
    if ((argc == 1) || (argc > 4)) {
        fprintf(stderr, "Usage: %s <STACK|FIFO|HEAP> [start] [goal]\n", argv[0]);
        return EXIT_FAILURE;
    }
    fringetype = 0;
    if ((strcmp(argv[1], "STACK") == 0) || (strcmp(argv[1], "LIFO") == 0)) {
        fringetype = STACK;
    } else if (strcmp(argv[1], "FIFO") == 0) {
        fringetype = FIFO;
    } else if ((strcmp(argv[1], "HEAP") == 0) || (strcmp(argv[1], "PRIO") == 0)) {
        fringetype = HEAP;
    }
    if (fringetype == 0) {
        fprintf(stderr, "Usage: %s <STACK|FIFO|HEAP> [start] [goal]\n", argv[0]);
        return EXIT_FAILURE;
    }
    start = 0;
    goal = 42;
    if (argc == 3) {
        goal = atoi(argv[2]);
    } else if (argc == 4) {
        start = atoi(argv[2]);
        goal = atoi(argv[3]);
    }
    printf("Problem: route from %d to %d\n", start, goal);
    search(fringetype, start, goal);
    return EXIT_SUCCESS;
}

/* This function prints the final path to the goal */
void printPath(Fringe parent, int goal) {
    int cost = 0;
    int length = 0;
    int numbers[parent.size]; //stores the intermediate numbers between the start and goal
    int operators[parent.size]; //stores the intermediate operations between the start and goal
    State state;
    for (int i = 0; i < parent.size; i++) {
        numbers[i] = operators[i] = 0;
    }
    while (!isEmptyFringe(parent)) {
        parent = removeFringe(parent, &state);
        /* is the current value is the current goal? */
        if (state.value == goal) {
            numbers[length] = state.value;
            /* check for operations using parent of the state*/
            if (state.value - 1 == state.parent) {
                operators[length] = PLUS_ONE;
                cost++;
            } else if (state.value + 1 == state.parent) {
                operators[length] = MINUS_ONE;
                cost++;
            } else if (state.value != 0) { //we avoid the case when 0*number or 0/number == 0
                if (state.value / 2 == state.parent) {
                    operators[length] = MULT_TWO;
                    cost += 2;
                } else if (state.value * 2 == state.parent) {
                    operators[length] = DIV_TWO;
                    cost += 3;
                } else if (state.value / 3 == state.parent) {
                    operators[length] = MULT_THREE;
                    cost += 2;
                } else if (state.value * 3 == state.parent) {
                    operators[length] = DIV_THREE;
                    cost += 3;
                }
            }
            length++;
            /* update the goal */
            goal = state.parent;
        }
    }
    /* reverse the order to display the path in ascending order from start to goal*/
    readReverse(numbers, operators, length);
    printf("\nlength = %d, cost = %d\n", length, cost);
    deallocFringe(parent);
}

/* This functions helps printPath function to display the path in the correct order */
void readReverse(int numbers[], int operators[], int length) {
    for (int i = length - 1; i >= 0; i--) {
        switch (operators[i]) {
            case PLUS_ONE:
                printf("(+1)-> ");
                break;
            case MINUS_ONE:
                printf("(-1)-> ");
                break;
            case MULT_TWO:
                printf("(*2)-> ");
                break;
            case MULT_THREE:
                printf("(*3)-> ");
                break;
            case DIV_TWO:
                printf("(/2)-> ");
                break;
            case DIV_THREE:
                printf("(/3)-> ");
                break;
            default:
                break;
        }
        printf("%d ", numbers[i]);
    }
}

Fringe insertValidSucc(Fringe fringe, int value, int parent, int priority) {
    State s;
    if ((value < 0) || (value > RANGE)) {
        /* ignore states that are out of bounds */
        return fringe;
    }
    s.value = value;
    s.parent = parent;
    s.priority = priority;
    return insertFringe(fringe, s);
}


void search(int mode, int start, int goal) {
    Fringe fringe, parent;
    State state;
    int goalReached = 0;
    int visited = 0;
    int value = 0, cost = 0;
    parent = makeFringe(LIFO);
    fringe = makeFringe(mode);
    state.value = start;
    state.parent = start;
    state.priority = 0;
    fringe = insertFringe(fringe, state);
    while (!isEmptyFringe(fringe)) {
        /* get a state from the fringe */
        fringe = removeFringe(fringe, &state);
        parent = insertFringe(parent, state);
        visited++;
        value = state.value;
        cost = state.priority;
        /* is state the goal? */
        if (value == goal) {
            goalReached = 1;
            break;
        }
        if (value < goal) {
            /* The cost for +1 is 1*/
            fringe = insertValidSucc(fringe, value + 1, value, cost + 1); /* rule n->n + 1      */
            if (value != 0 && value < (goal / 2)) {
                /* The cost for *2 is 2*/
                fringe = insertValidSucc(fringe, 2 * value, value, cost + 2); /* rule n->2*n        */
                /* The cost for *3 is 2*/
                fringe = insertValidSucc(fringe, 3 * value, value, cost + 2); /* rule n->3*n        */
            }
        } else {
            /* The cost for -1 is 1*/
            fringe = insertValidSucc(fringe, value - 1, value, cost + 1); /* rule n->n - 1      */
            if (value != 0 && value > (goal / 2)) {
                /* The cost for /2 is 3*/
                fringe = insertValidSucc(fringe, value / 2, value, cost + 3); /* rule n->floor(n/2) */
                /* The cost for /3 is 3*/
                fringe = insertValidSucc(fringe, value / 3, value, cost + 3); /* rule n->floor(n/3) */
            }
        }
    }
    printPath(parent, goal);
    if (goalReached == 0) {
        printf("goal not reachable ");
    } else {
        printf("goal reached ");
    }
    showStats(fringe);
    deallocFringe(fringe);
}
