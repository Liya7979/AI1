#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define N 500   /* N times N chessboard */

int actions[8][2] = {  /* knight moves */
        {-2, -1},
        {-2, 1},
        {-1, -2},
        {-1, 2},
        {1,  -2},
        {1,  2},
        {2,  -1},
        {2,  1}
};
//counts the number of processed states
unsigned long statesVisited = 0;
typedef struct NextState {
    int row;
    int column;
    float func;
} NextState;
int moves = 0;

double effectiveBranchingFactor(unsigned long states, int d) {
    /* approximates such that N=\Sum+{i=1}^{d} b^i */
    double lwb = 1;
    double upb = pow(states, 1.0 / d);
    while (upb - lwb > 0.000001) {
        double mid = (lwb + upb) / 2.0;
        /* the following test makes use of the geometric series */
        if (mid * (1 - pow(mid, d)) / (1 - mid) <= states) {
            lwb = mid;
        } else {
            upb = mid;
        }
    }
    return lwb;
}

int isValidLocation(int x, int y) {
    return (0 <= x && x < N && 0 <= y && y < N);
}

int min(int a, int b) {
    return (a < b ? a : b);
}

//admissible heuristics no.1 (h) - calculates the real distance not considering the grid
float euclidDistance(int row, int column, int rowGoal, int columnGoal) {
    int dx = rowGoal - row;
    int dy = columnGoal - column;
    return (float) sqrt(dx * dx + dy * dy);
}

//admissible heuristics no.2 (h) - calculates the minimal distance to the goal including diagonal moves
//divided by three because each time the knight moves, it passes 3 squares.
float diagonalDistance(int row, int column, int rowGoal, int columnGoal) {
    int dx = abs(row - rowGoal);
    int dy = abs(column - columnGoal);
    return (float) (dx + dy - min(dx, dy)) / 3;
}

//checks if the state is located close to the goal, according to 4.2 Figure 2
int isClose(int row, int column, int rowGoal, int columnGoal) {
    if (((row + 1) == rowGoal && (column + 1) == columnGoal) ||
        ((row + 2) == rowGoal && column == columnGoal) ||
        (row == rowGoal && column + 2 == columnGoal) ||
        ((row - 1) == rowGoal && (column - 1) == columnGoal) ||
        ((row - 2) == rowGoal && column == columnGoal) ||
        (row == rowGoal && column - 2 == columnGoal)) {
        moves += 2;
        statesVisited += 2;
        return 1;
    }
    if ((row + 1 == rowGoal && column == columnGoal) ||
        (row == rowGoal && column + 1 == columnGoal) ||
        (row - 1 == rowGoal && column == columnGoal) ||
        (row == rowGoal && column - 1 == columnGoal)) {
        moves++;
        statesVisited++;
        return 1;
    }
    return 0;
}

//A* algorithm recursive
void AS(float cost, int row, int column, int rowGoal, int columnGoal, int heuristicType) {
    int act;
    NextState nextState; //represents the next state to be explored
    if (row == rowGoal && column == columnGoal || isClose(row, column, rowGoal, columnGoal)) {
        return; // goal reached
    }
    float minimal = 1000;
    for (act = 0; act < 8; act++) { //allowed actions
        int r = row + actions[act][0];
        int c = column + actions[act][1];
        if (isValidLocation(r, c)) {
            //calculates f = g + h
            float f = (heuristicType == 1 ? 1 + euclidDistance(r, c, rowGoal, columnGoal) :
                       1 + diagonalDistance(r, c, rowGoal, columnGoal));
            //if the current state is optimal, assign to nextState
            if (f < minimal) {
                statesVisited++;
                minimal = f;
                nextState.func = f;
                nextState.row = r;
                nextState.column = c;
            }
        }
    }
    moves++;
    AS(nextState.func, nextState.row, nextState.column, rowGoal, columnGoal, heuristicType);
}

int main(int argc, char *argv[]) {
    int heuristicType = 1;
    int x0, y0, x1, y1;
    do {
        printf("Start location (x,y) = ");
        fflush(stdout);
        scanf("%d %d", &x0, &y0);
    } while (!isValidLocation(x0, y0));
    do {
        printf("Goal location (x,y)  = ");
        fflush(stdout);
        scanf("%d %d", &x1, &y1);
        printf("Choose heuristic (Euclid distance = 1)|(Diagonal distance = 2)\n");
        // choose heursitic: 1 - euclidDistance, other value - diagonalDistance
        printf("Heuristic type:");
        fflush(stdout);
        scanf("%d", &heuristicType);
    } while (!isValidLocation(x1, y1));
    AS(N, x0, y0, x1, y1, heuristicType);
    printf("#path length: %d\n", moves);
    printf("#visited states: %lu\n", statesVisited);
    printf("#branching factor: %lf \n", effectiveBranchingFactor(statesVisited, moves));
    return 0;
}
