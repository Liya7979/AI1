#include <stdio.h>
#include <stdlib.h>


#define RANGE 1000000
//ACTIONS:
#define PLUS_ONE 1 //(+1)
#define MINUS_ONE 2 //(-1)
#define MULT_TWO 3 //(*2)
#define MULT_THREE 4 //(*3)
#define DIV_TWO 5 //(/2)
#define DIV_THREE 6 //(/3)

/* Rerpesents recursive DLS algorithm at some limit indicated by IDS*/
int recursiveDLS(int value, int start, int goal, int d, int limit, int cost, int *visitedStates);

/* Checks if the value is in the range 0...1000000*/
int validSucc(int number, int operation);

/* Computes the value accroding to the action performed*/
int computeValue(int n, int operation);

/* Computes the cost after performing some action */
int computeCost(int operation, int cost);

/* Prints the number of visited nodes, cost and limit*/
void printStats(int start, int goal, int visitedStates, int limit, int cost);

int main(int argc, char *argv[]) {
    int start, goal;
    int limit = 0, depth = 0;
    int visitedStates = 0, cost = 0;


    printf("Problem: ");
    scanf("%d %d", &start, &goal);
    printf("limit=0");
    fflush(stdout);
    /* IDS algorithm preforms DLS for increasing limits */
    while (recursiveDLS(start, start, goal, depth, limit, cost, &visitedStates) == 0) {
        limit++;
        printf(",%d", limit);
        fflush(stdout);
    }
    printf("\n");

    return 0;
}

int recursiveDLS(int value, int start, int goal, int d, int limit, int cost, int *visitedStates) {
    (*visitedStates)++;

    if (value == goal) { //prints path and statistics
        printStats(start,goal, *visitedStates, limit, cost);
        return 1;
    }
    if (d == limit) { //the limit should be increased, cutoff reached
        return 0;
    }
    for (int action = 1; action <= 6; action++) { //perform the action
        if (validSucc(value, action) &&
            recursiveDLS(computeValue(value, action), start, goal, d + 1, limit, computeCost(action, cost),
                         visitedStates) == 1) {
            return 1;
        }
    }
}

int validSucc(int number, int operation) {
    switch (operation) {
        case PLUS_ONE:
            if (number + 1 >= RANGE) { // n + 1
                return 0;
            }
            break;
        case MINUS_ONE:
            if (number - 1 < 0) { // n - 1
                return 0;
            }
            break;
        case MULT_TWO:
            if (2 * number >= RANGE) { // n * 2
                return 0;
            }
            break;
        case MULT_THREE:
            if (3 * number >= RANGE) { // n * 3
                return 0;
            }
            break;
        case DIV_TWO: // n/2, the result will always be in RANGE
        case DIV_THREE: // n/3, same as n/2
            return 1;
        default:
            break;
    }
    return 1;
}

int computeValue(int n, int operation) {
    switch (operation) {
        case PLUS_ONE:
            return (n + 1);
        case MINUS_ONE:
            return (n - 1);
        case MULT_TWO:
            return (n * 2);
        case MULT_THREE:
            return (n * 3);
        case DIV_TWO:
            return (n / 2);
        case DIV_THREE:
            return (n / 3);
    }
}

int computeCost(int operation, int cost) {
    switch (operation) {
        case PLUS_ONE:
        case MINUS_ONE:
            return (cost + 1);
        case MULT_TWO:
        case MULT_THREE:
            return (cost + 2);
        case DIV_TWO:;
        case DIV_THREE:
            return (cost + 3);
    }
}
void printStats(int start, int goal, int visitedStates, int limit, int cost) {
    printf("\nProblem: route from %d to %d\n", start, goal);
    printf("Goal reached (%d nodes visited)\n", visitedStates);
    printf("#limit: %d\n", limit);
    printf("#cost: %d\n", cost);
}





