# AI1
Lab assignments of the course AI1 (search algorithms)

The main skills learnt during this course are:
- Identifying different types of agents and environments
- Describing, applying and implementing main AI algorithms for uninformed, informed, adversarial and local search
- Specifying constraint satisfaction problems and describing the main algorithms at work in CSP solvers
- Describing and applying the main techniques for automatic deduction, reasoning and problem solving in propositional and predicate logic

Lab1 and lab2 assignments are supplied in the pdf file inside each of the corresponding folders.
