#include <stdio.h>
#include <stdlib.h>

#define MAX 1
#define MIN (-1)

#define INFINITY 9999999

typedef struct Best { //struct of pair move+valuation
    int value;
    int move;
} Best;

typedef struct TT {
    int value;
    int state;
    int turn;
} TT;

TT tt[100]; //transposition table
int size = 0; //size of transposition table

/* initializes the global array of transposition table*/
void initialize() {
    for (int i = 0; i < 100; i++) {
        (tt[i]).value = 0;
        (tt[i]).state = -1;
        (tt[i]).turn = 0;
    }
}

/* computes the index of the value of some state if it is in the transposition table, otherwise returns -1*/
int transpositionValue(int state) {
    for (int i = 0; i < size; i++) {
        if (tt[i].state == state) {
            return i;
        }
    }
    return -1;
}

int negamaxValue(int state, int turn, int turnTranspositionTable) {
    int i=-1;  // represents the index of the state at transposition table
    /* terminal state  */
    if (state == 1) {
        return -turn;
    }
    int move, negamax = -INFINITY;
    /* non-terminal state */
    for (move = 1; move <= 3; move++) {
        if (state - move > 0) { /* legal move */
            if (turnTranspositionTable == 1){
                 i = transpositionValue(state - move);
            }
            if (i == -1) { /* if the value is not in the transposition table */
                int m = -negamaxValue(state - move, -turn, turnTranspositionTable);
                if ((m > negamax)) {
                    negamax = m;
                    if (turnTranspositionTable == 1) {
                        tt[size].state = state - move;
                        tt[size].value = negamax;
                        tt[size].turn = turn;
                        size++;
                    }
                }
            } else { /* value is in the transposition table, do nothing */
                if (tt[i].turn == turn) {
                    negamax = tt[i].value;
                } else {
                    negamax = turn * tt[i].value; //check whether the value is used at the same turn
                }

            }
        }
    }
    return negamax;
}


Best minimaxDecision(int state, int turn, int turnTranspositionTable) {
    Best best; /* returns the pair of best move and its valuation */
    int move, max, min;
    if (turn == MAX) {
        max = -INFINITY;
        for (move = 1; move <= 3; move++) {
            if (state - move > 0) { /* legal move */
                int m = negamaxValue(state - move, turn, turnTranspositionTable);
                if (m > max) {
                    max = m;
                    best.value = max;
                    best.move = move;
                }
            }
        }
        return best;
    }
    /* turn == MIN */
    min = INFINITY;
    for (move = 1; move <= 3; move++) {
        if (state - move > 0) { /* legal move */
            int m = -negamaxValue(state - move, -turn,turnTranspositionTable);
            if (m < min) {
                min = m;
                best.value = min;
                best.move = move;
            }

        }
    }
    return best;
}

void playNim(int state, int turnTranspositionTable) {
    int turn = 1;
    if (turnTranspositionTable == 1){
        initialize();
    }
    while (state != 1) {
        int action = minimaxDecision(state, turn, turnTranspositionTable).move;
        int value = minimaxDecision(state, turn, turnTranspositionTable).value;
        printf("%d: %s takes %d, value is %d\n", state,
               (turn == MAX ? "Max" : "Min"), action, value);
        state = state - action;
        turn = -turn;
    }
    printf("1: %s looses\n", (turn == MAX ? "Max" : "Min"));
}

int main(int argc, char *argv[]) {
    if ((argc != 3) || (atoi(argv[1]) < 3)) {
        fprintf(stderr, "Usage: %s <number of sticks>, where ", argv[0]);
        fprintf(stderr, "Usage: %s <transpositon table option> ", argv[2]);
        fprintf(stderr, "<number of sticks> must be at least 3!\n");
        return -1;
    }

    playNim(atoi(argv[1]), atoi(argv[2]));

    return 0;
}
